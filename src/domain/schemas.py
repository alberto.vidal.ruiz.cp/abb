from pydantic import BaseModel
from typing import Optional

class User(BaseModel):
    id: Optional[int]
    hash: str

    class Config:
        orm_mode = True