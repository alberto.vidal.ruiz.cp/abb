from src.domain import schemas

from src.infraestructure import orm, repository



async def post_user(repo: repository.CentralesRepository, user: schemas.User):
    userindb = orm.Users(hash = user.hash)
    repo.add(userindb)
    repo.commit()


async def count_unique_users(repo: repository.CentralesRepository):
    return repo.count_unique_users(orm.Users)


async def is_unique_user(repo: repository.CentralesRepository, user_hash: str):
    is_unique = repo.is_unique_user(orm.Users, user_hash)
    
    return is_unique