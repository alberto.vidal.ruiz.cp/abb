import abc


class AbstractRepository(abc.ABC):

    @abc.abstractmethod
    def add(self, model):
        raise NotImplementedError

    @abc.abstractmethod
    def get(self, id):
        raise NotImplementedError


class SqlAlchemyRepository(AbstractRepository):

    def __init__(self, session):
        self.session = session()

    def add(self, model):
        self.session.add(model)

    def add_all(self, models):
        self.session.add_all(models)

    def get(self, model, id):
        return self.session.query(model).filter_by(id=id).first()

    def getAll(self, model):
        return self.session.query(model).all()

    def merge(self, model):
        return self.session.merge(model)

    def delete(self, model):
        return self.session.delete(model)

    def commit(self):
        self.session.flush()
        self.session.commit()

    def rollback(self):
        self.session.refresh()
        self.session.rollback()
    
    def refresh(self):
        self.session.refresh()


class CentralesRepository(SqlAlchemyRepository):

    def count_unique_users(self, user):
        return self.session.query(user).count()

    def is_unique_user(self, user, user_hash):
        return self.session.query(user).filter(user.hash == user_hash).first()