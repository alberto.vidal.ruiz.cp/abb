import os

from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.session import Session
from sqlalchemy.ext.declarative import declarative_base

from datetime import datetime

# "postgresql://{user}:{password}@{host}:{port}/{db_name}"
host = os.environ.get('DB_HOST', 'localhost')
port = 5432
password = os.environ.get('DB_PASSWORD', 'abb1234')
user, db_name = 'postgres', 'abb-db'
DATABASE_URI = f"postgresql://{user}:{password}@{host}:{port}/{db_name}"


engine = create_engine(
    url = DATABASE_URI,
    echo = True)

# metadata = MetaData(engine)
DEFAULT_SESSION_FACTORY = sessionmaker(
    bind=engine,
    autocommit=False,
    autoflush=False,
    expire_on_commit=False
)

Base = declarative_base()

class Users(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, autoincrement=True)
    hash = Column(String)


Base.metadata.create_all(bind=engine)