FROM python:3.9

RUN apt-get -y update

EXPOSE 80/tcp
EXPOSE 8000/tcp

COPY . /app
WORKDIR /app

RUN pip install -r requirements.txt

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]