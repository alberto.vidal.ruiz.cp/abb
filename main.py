import json


from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from datetime import datetime

from src.domain import schemas
from src.services import services
from src.infraestructure import repository, orm

# DEBUGGING ORM LOG
import logging
logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)


CONFIG_FILEPATH = "./config.json"

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

CONFIG_FILEPATH = "./config.json"
with open(CONFIG_FILEPATH) as file:
    _config = json.load(file)


@app.get("/")
async def root(request: Request):
    users_repo  = repository.CentralesRepository(session = orm.DEFAULT_SESSION_FACTORY)
    user_hash = await hash_user(
        ip = request.client.host,
        user_agent = request.headers.get('User-Agent'),
        date = datetime.today().toordinal())
    user_data = {
        "hash": user_hash
    }
    user = schemas.User(**user_data)
    
    is_unique = await services.is_unique_user(users_repo, user_hash)
    if not is_unique:
        await services.post_user(users_repo, user)
    # count unique sessions
    total_uniques_users = await services.count_unique_users(users_repo)
    return {'total_uniques_users':  total_uniques_users}


@app.get("/version")
async def get_version():
    return {'version': _config['version']}


async def hash_user(ip, user_agent, date):
    return str(hash(str(ip) + user_agent + str(date)))




if __name__ == '__main__':
    import uvicorn

    with open(CONFIG_FILEPATH) as file:
        config = json.load(file)

    uvicorn.run("main:app", 
        host=config['ipAddress'],
        port=config['port'],
        proxy_headers= False,
        forwarded_allow_ips='*',
        reload=False)