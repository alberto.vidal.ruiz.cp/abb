# ABB TECHNICAL TEST

ABB TECHNICAL TEST is an app that displays the number of unique visitors to a GATEWAY API. The architecture of the system is composed of:
- Frontend: Any web browser, http compatible with JSON.
- Backend: Built on [Python 3.9](https://www.python.org/downloads/) for DEBIAN GNU/LINUX distro.
- BBDD: Relational database with [PostegreSQL 12.10](https://www.postgresql.org/docs/12/index.html).

The stack used has been:

- [FastAPI](https://fastapi.tiangolo.com/): Main framework on a Clean Architecture structure.
- [SQLAlchemy](https://www.sqlalchemy.org/): ORM for interaction with the BBDD
- [Docker + Docker Compose](https://docs.docker.com/get-started/): As a virtualisation, test and local deployment environment.
- [Git + GitLab + GCP](https://docs.gitlab.com/): Version Control & CI/CD .

## Development environment

### Dependencies

It is recommended to work in a linux development environment with, Python, Git, Docker, Terraform and the following packages installed:

```bash
sudo apt-get install python3 python-dev python3-dev \
    build-essential libssl-dev libffi-dev \
    libxml2-dev libxslt1-dev zlib1g-dev \
    python-pip
```

```bash
python --version
```

```bash
witch python
```

```bash
pip --version
```

```python3
python -m pip install --upgrade pip
```

Creating and working in a [virtual environment]((https://virtualenv.pypa.io/en/latest/)):

```python3
pip install virtualenv
```

```python3
python -m venv .venv
```

```bash
source venv/bin/activate
```

Finally install the project dependencies, available in the requirements.txt :

```python3
pip install -r requirements.txt
```

### RUNNING DEV WITH DOCKER

While developing the app locally, it is recommended to run the following containers:

CREATE BBDD ENV
```bash
docker run --name postgres -p 5432:5432 -e POSTGRES_DB=abb-db -e POSTGRES_PASSWORD=abb1234 -d postgres
```

CREATE API REST ENV:

```bash
docker build -t fastapi .
```

```bash
docker run -d --name fastapi.1.0.0 -p 8000:8000 fastapi
```

## Deployment

### RUNNING DEV WITH DOCKER

For deployment in the sandbox, it is recommended to use docker compose, defined in docker-compose.yml:

```bash
docker-compose up 
```


### CI/CD WITH GITLAB + GCP

The first thing is to have an account created in gitlab and another validated account in Google Clouad Platform. 

The products we are going to use from both are Gitlab CI/CD to automate the building of docker containers and Cloud Run for deployment in the Google cloud.

the whole process is available from the gitlab-cli.yml file.







